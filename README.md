# Sample Django Project
This is a sample Django project.

## Populate the database
In order to populate the database with some products, a fixture has been added to the project. A superuser with the `id = 1` must have been created previously. Execute the following command:

```
python manage.py loaddata shoes/api/fixtures/data_fixture.json
```

## Placing an order
The following payload must be sent as a POST request to `/orders/` endpoint.
```
{
    "address": "A random address 34",
    "postalCode": "08080",
    "city": "Madrid",
    "country": "España",
    "items": [
        {
            "id": 4,
            "quantity": 2
        }
    ]
    "created": "2021-07-10T04:25:01.304451Z"
}
```
