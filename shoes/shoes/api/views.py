from django.contrib.auth.models import User
from django.shortcuts import render
from rest_framework import permissions, viewsets
from shoes.api.models import Order, Product
from shoes.api.serializers import (OrderSerializer,
                                   ProductSerializer, UserSerializer)


# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]

class ProductViewSet(viewsets.ModelViewSet):
    """
    Endpoint to list and view products.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get_permissions(self):
        """
        Only admin users can create, update or delete. Listing and retrieving is public.
        """
        if self.action in ['list', 'retrieve']:
            return []

        permission_classes = [permissions.IsAdminUser]
        return [permission() for permission in permission_classes]

class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        orders = Order.objects.all()
        if self.request.user.is_superuser:
            return orders
        return orders.filter(user=self.request.user)
