from datetime import datetime
from django.db import transaction
from django.contrib.auth.models import User
from shoes.api.models import Order, OrderProduct, Product
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email']


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = ['name', 'description', 'barCode', 'price', 'inStock', 'created']

    barCode = serializers.CharField(source='bar_code')
    inStock = serializers.SerializerMethodField(method_name='is_in_stock')

    def is_in_stock(self, instance):
        return instance.stock > 0


class OrderItemSerializer(serializers. HyperlinkedModelSerializer):
    class Meta:
        model = OrderProduct
        fields = ['id', 'barCode', 'name', 'price', 'quantity']

    id = serializers.PrimaryKeyRelatedField(source='product', queryset=Product.objects.all())
    barCode = serializers.ReadOnlyField(source='product__bar_code')
    name = serializers.ReadOnlyField(source='product__name')
    price = serializers.ReadOnlyField()


class OrderSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Order
        fields = ['url', 'number', 'address', 'postalCode', 'city', 'country', 'payment', 'items', 'total', 'created', 'createdBy']

    number = serializers.CharField(source='order_number', read_only=True)
    postalCode = serializers.CharField(source='postal_code')
    payment = serializers.CharField(source='payment_status', read_only=True)
    total = serializers.CharField(source='total_price', read_only=True)
    createdBy = serializers.PrimaryKeyRelatedField(source='user', read_only=True)
    items = OrderItemSerializer(many=True)

    @transaction.atomic
    def create(self, validated_data):
        request = self.context.get('request')
        validated_data['user'] = request.user
        validated_data['order_number'] = datetime.now().strftime("%Y%m%d%H%M%S")

        validated_items = validated_data.pop('items', [])
        order = super().create(validated_data)

        items = []
        total_price = 0
        for product_item in validated_items:
            product_item['price'] = product_item.get('product').price
            product_item['order'] = order
            total_price += product_item.get('price') * product_item.get('quantity')
            items.append(OrderProduct(**product_item))

        OrderProduct.objects.bulk_create(items)
        order.total_price = total_price
        order.save()

        return order
